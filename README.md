# Scarlet Iron

Demonstrates capabilities of the React Query package

## Run locally

1. Install dependencies

`yarn install`

2. Start local host

`yarn run serve-json`

3. Start app

`yarn start`