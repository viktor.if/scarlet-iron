import axios from "axios"
import { useQuery } from "react-query"

const fetchUserByEmail = (email) => {
  return axios.get(`http://localhost:4000/users/${email}`)
}

const fetchTopicsByChannelId = (channelId) => {
  return axios.get(`http://localhost:4000/channels/${channelId}`)
}

export const DependentQueriesPage = ({ email }) => {
  const { data: user } =  useQuery(['user', email], () => fetchUserByEmail(email))
  const channelId = user?.data.channelId

  useQuery(
    ['topics', channelId],
    () => fetchTopicsByChannelId(channelId),
    { enabled: !!channelId }
  )

  return <div>DependentQueries</div>
}