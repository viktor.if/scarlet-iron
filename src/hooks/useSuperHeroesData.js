import { useMutation, useQuery, useQueryClient } from 'react-query'
import { request } from '../utils/axios-utils'

const fetchSuperHeros = () => {
  return request({ url: '/superheroes'})
}

const addSuperHero = (hero) => {
  return request({ url: '/superheroes', method: 'post', data: hero })
}

export const useSuperHeroesData = (onSuccess, onError) => {
  // const select = (data) => {
  //   return data.data.map((hero) => hero.name)
  // }

  return useQuery(
    'super-heroes',
    fetchSuperHeros,
    {
      // cacheTime: 5000, // default 30000
      // staleTime: 30000, // default 0
      // refetchOnMount: true, // default true
      refetchOnWindowFocus: false, // default true
      // refetchInterval: 2000, // default false
      // refetchIntervalInBackground: true, // default false
      // enabled: false,
      onSuccess,
      onError,
      // select,
    },
  )
}

export const useAddSuperHeroData = () => {
  const queryClient = useQueryClient()

  return useMutation(addSuperHero, {
    // onSuccess: (data) => {
    //   // queryClient.invalidateQueries('super-heroes')
    //   queryClient.setQueryData('super-heroes', (oldQueryData) => {
    //     return {
    //       ...oldQueryData,
    //       data: [
    //         ...oldQueryData.data,
    //         data.data,
    //       ]
    //     }
    //   })
    // }
    onMutate: async (newHero) => {
      await queryClient.cancelQueries('super-heroes')
      const prevHeroData = queryClient.getQueriesData('super-heroes')

      queryClient.setQueryData('super-heroes', (oldQueryData) => {
        return {
          ...oldQueryData,
          data: [
            ...oldQueryData.data,
            {
              id: oldQueryData?.data?.length + 1,
              ...newHero
            },
          ]
        }
      })

      return { prevHeroData }
    },
    onError: (_error, _hero, context) => {
      queryClient.setQueryData('super-heroes', context.prevHeroData)
    },
    onSettled: () => {
      queryClient.invalidateQueries('super-heroes')
    }
  })
}